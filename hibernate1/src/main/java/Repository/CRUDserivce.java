package Repository;

import org.aspectj.apache.bcel.util.Repository;

import java.util.List;

public interface CrudRepository<T, ID> extends Repository<T, ID> {
        List<T> findAll();
        T findById(U id);
        int insert(T object);
        int update(T object);
        int delete(T object);
        int deleteById(U id)
}
